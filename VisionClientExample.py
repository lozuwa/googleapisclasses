import io, os
from google.cloud import vision

# Create a client (check the credentials)
vision_client = vision.Client('sophi')

# the name of the image file to annotate
file_name = os.path.join(os.path.dirname(__file__), 'c:/Users/HP/Pictures/Others/click.png')

print('file_name: ', file_name)

with io.open(file_name, 'rb') as image_file:
	content = image_file.read()
	image = vision_client.image(content=content) 

# Performs label detection
labels = image.detect_labels()

# Print the labels 
for each in labels:
	print(each.description, each.score)