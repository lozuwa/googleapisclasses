import io, os, sys 
from google.cloud import vision 

class GoogleVision:
	def __init__(self, precision = 0.50):
		self.vision_client = vision.Client('sophi')
		self.precision = precision

	def predict(self, image_path):
		# Check file 
		if os.path.isfile(image_path):
			pass
		else:
			raise Exception('File does not exist')

		# Read file 
		with io.open(image_path, 'rb') as image_file:
			content = image_file.read()
			image = self.vision_client.image(content = content)

		# Label detection
		labels = image.detect_labels()

		# Store labels
		return_labels = {each.description: str(each.score)[:4] for each in labels if each.score > self.precision}

		# Return labels 
		return return_labels
